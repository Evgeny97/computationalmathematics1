//Created by Evgeny on 05.09.2017.

public class Main {
    public static void main(String[] args) {

        //JFrame.setDefaultLookAndFeelDecorated(true);
        GUI gui = new GUI();
        javax.swing.SwingUtilities.invokeLater(gui::createGUI);

    }
}
