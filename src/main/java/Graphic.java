//Created by Evgeny on 06.09.2017.

import equations.Equation;
import equations.ThirdDegreeEquation;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.markers.SeriesMarkers;

import javax.swing.*;
import java.awt.*;

public class Graphic {
    private int graphicWidth = 600;

    public void createGraphic(ThirdDegreeEquation equation, double minX, double maxX) {
        XYChart chart = new XYChartBuilder().xAxisTitle("X").yAxisTitle("Y").width(600).height(400).build();

        chart.getStyler().setXAxisMin(minX);
        chart.getStyler().setXAxisMax(maxX);

        double[] xData = getXData(minX, maxX);
        String seriesName = "x^3+" + String.valueOf(equation.getSecondDegree()) + "*x^2+" +
                String.valueOf(equation.getFirstDegree()) + "*x+" + String.valueOf(equation.getZeroDegree());
        XYSeries series = chart.addSeries(seriesName, xData, getYDate(equation, xData));
        series.setMarker(SeriesMarkers.NONE);
        JFrame frame = new JFrame("График");
        Container container = frame.getContentPane();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        javax.swing.SwingUtilities.invokeLater(() -> {
            frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            XChartPanel<XYChart> chartPanel = new XChartPanel<>(chart);
            container.add(chartPanel);
            container.add(new JLabel(equation.getRootsAsString()));
            frame.pack();
            frame.setVisible(true);
        });
    }

    private double[] getXData(double minX, double maxX) {

        double[] x = new double[graphicWidth];
        double step = (maxX - minX) / graphicWidth;
        for (int i = 0; i < graphicWidth; i++) {
            x[i] = minX + i * step;
        }
        return x;
    }

    private double[] getYDate(Equation equation, double[] xData) {

        double[] y = new double[graphicWidth];
        for (int i = 0; i < y.length; i++) {
            y[i] = equation.calculate(xData[i]);
        }
        return y;
    }
}
