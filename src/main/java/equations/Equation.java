package equations;
//Created by Evgeny on 08.09.2017.

public abstract class Equation {
    abstract public double calculate(double x);
}
