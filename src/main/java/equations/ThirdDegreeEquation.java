//Created by Evgeny on 08.09.2017.
package equations;

import java.util.HashMap;
import java.util.Map;

public class ThirdDegreeEquation extends Equation {
    private double secondDegree;
    private double firstDegree;
    private double zeroDegree;
    private double epsilon;
    private double delta;

    public ThirdDegreeEquation(double secondDegree, double firstDegree, double zeroDegree, double epsilon, double delta) {
        this.secondDegree = secondDegree;
        this.firstDegree = firstDegree;
        this.zeroDegree = zeroDegree;
        this.epsilon = epsilon;
        this.delta = delta;
    }

    public double getSecondDegree() {
        return secondDegree;
    }

    public double getFirstDegree() {
        return firstDegree;
    }

    public double getZeroDegree() {
        return zeroDegree;
    }

    public double calculate(double x) {
        double xInSecondDegree = x * x;
        return xInSecondDegree * x + xInSecondDegree * secondDegree + x * firstDegree + zeroDegree;
    }

    private SecondDegreeEquation getDerivative() {
        return new SecondDegreeEquation(3,
                secondDegree * 2, firstDegree, epsilon);
    }

    private int rootsCount() {
        SecondDegreeEquation derivative = getDerivative();
        if (derivative.getDiscriminant() < epsilon) {
            return 1;
        }
        double[] derivativeRoots = derivative.getRoots();
        if (calculate(derivativeRoots[0]) * calculate(derivativeRoots[1]) >= epsilon * epsilon) {
            return 1;
        } else {
            return 3;
        }
    }

    private double findRootBetween(double minX, double maxX) {

        if (Math.abs(calculate(minX)) < epsilon) {
            return minX;
        }
        if (Math.abs(calculate(maxX)) < epsilon) {
            return maxX;
        }
        if (!(calculate(minX) * calculate(maxX) < -epsilon * epsilon)) {
            throw new RuntimeException("ошибка");
        }

        double centreX = (minX + maxX) / 2;

        if (calculate(minX) * calculate(centreX) < -epsilon * epsilon) {
            return findRootBetween(minX, centreX);
        }
        if (calculate(minX) * calculate(centreX) > epsilon * epsilon) {
            return findRootBetween(centreX, maxX);
        }
        return centreX;
    }

    private double findRootBefore(double maxX) {
        if (Math.abs(calculate(maxX)) < epsilon) {
            return maxX;
        }
        double minX = maxX - delta;
        if (calculate(maxX) > epsilon) {
            while (calculate(minX) > epsilon) {
                minX -= delta;
            }
        } else {
            while (calculate(minX) < -epsilon) {
                minX -= delta;
            }
        }
        return findRootBetween(minX, maxX);
    }

    private double findRootAfter(double minX) {
        if (Math.abs(calculate(minX)) < epsilon) {
            return minX;
        }
        double maxX = minX + delta;
        if (calculate(minX) > epsilon) {
            while (calculate(maxX) > epsilon) {
                maxX += delta;
            }
        } else {
            while (calculate(maxX) < epsilon) {
                maxX += delta;
            }
        }
        return findRootBetween(minX, maxX);
    }

    public String getRootsAsString() {
        HashMap<Double, Integer> roots = getRoots();
        String answer = "";
        roots.entrySet();
        for (Map.Entry<Double, Integer> root : roots.entrySet()) {
            answer += " Корень: " + String.format("%.4f", root.getKey()) + " кратности " + root.getValue().toString();
        }
        return answer;
    }

    public HashMap<Double, Integer> getRoots() {
        HashMap<Double, Integer> roots = new HashMap<>();
        if (rootsCount() == 1) {
            if (Math.abs(calculate(0)) < epsilon) {
                roots.put(0d, 1);
            } else if (calculate(0) < -epsilon) {
                roots.put(findRootAfter(0), 1);
            } else {
                roots.put(findRootBefore(0), 1);
            }
        } else {
            double[] derivativeRoots = getDerivative().getRoots();
            if (Math.abs(calculate(derivativeRoots[0])) <= epsilon && calculate(derivativeRoots[1]) < -epsilon) {
                roots.put(derivativeRoots[0], 2);
                roots.put(findRootAfter(derivativeRoots[1]), 1);
            } else if (Math.abs(calculate(derivativeRoots[1])) <= epsilon && calculate(derivativeRoots[0]) > epsilon) {
                roots.put(findRootBefore(derivativeRoots[0]), 1);
                roots.put(derivativeRoots[1], 2);
            } else if (Math.abs(calculate(derivativeRoots[0])) <= epsilon && Math.abs(calculate(derivativeRoots[1])) <= epsilon) {
                roots.put(derivativeRoots[0], 3);
            } else {
                roots.put(findRootBefore(derivativeRoots[0]), 1);
                roots.put(findRootBetween(derivativeRoots[0], derivativeRoots[1]), 1);
                roots.put(findRootAfter(derivativeRoots[1]), 1);
            }
        }
        return roots;
    }
}
