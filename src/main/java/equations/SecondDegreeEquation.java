package equations;
//Created by Evgeny on 08.09.2017.

public class SecondDegreeEquation extends Equation {
    private double secondDegree;
    private double firstDegree;
    private double zeroDegree;
    private double discriminant;
    private double epsilon;

    public SecondDegreeEquation(double secondDegree, double firstDegree, double zeroDegree, double epsilon) {
        this.secondDegree = secondDegree;
        this.firstDegree = firstDegree;
        this.zeroDegree = zeroDegree;
        this.epsilon = epsilon;
        discriminant = firstDegree * firstDegree - 4 * secondDegree * zeroDegree;
    }

    public double calculate(double x) {
        return x * x * secondDegree + x * firstDegree + zeroDegree;
    }

    public double getDiscriminant() {
        return discriminant;
    }

    public double[] getRoots() {
        double[] roots = new double[2];
        roots[0] = (-firstDegree - Math.sqrt(discriminant)) / (2 * secondDegree);
        roots[1] = (-firstDegree + Math.sqrt(discriminant)) / (2 * secondDegree);
        return roots;
    }
}
