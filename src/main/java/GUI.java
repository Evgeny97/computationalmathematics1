//Created by Evgeny on 06.09.2017.

import equations.ThirdDegreeEquation;

import javax.swing.*;
import java.awt.*;

public class GUI {
    private Graphic graphic = new Graphic();

    public void createGUI() {
        JFrame frame = new JFrame("Меню");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container container = frame.getContentPane();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        GridLayout layout = new GridLayout(0, 2, 4, 12);

        JPanel paramsPanel = new JPanel(layout);
        paramsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JLabel epsLabel = new JLabel("ε");
        epsLabel.setHorizontalAlignment(JLabel.CENTER);
        paramsPanel.add(epsLabel);
        JTextField epsilonTextField = new JTextField(String.valueOf(0.00000001));
        paramsPanel.add(epsilonTextField);

        JLabel deltaLabel = new JLabel("Δ");
        deltaLabel.setHorizontalAlignment(JLabel.CENTER);
        paramsPanel.add(deltaLabel);
        JTextField deltaTextField = new JTextField(String.valueOf(1));
        paramsPanel.add(deltaTextField);

        JLabel minXLabel = new JLabel("min x");
        minXLabel.setHorizontalAlignment(JLabel.CENTER);
        paramsPanel.add(minXLabel);
        JTextField minXTextField = new JTextField(String.valueOf(-10));
        paramsPanel.add(minXTextField);

        JLabel maxXLabel = new JLabel("max x");
        maxXLabel.setHorizontalAlignment(JLabel.CENTER);
        paramsPanel.add(maxXLabel);
        JTextField maxXTextField = new JTextField(String.valueOf(10));
        paramsPanel.add(maxXTextField);

        container.add(paramsPanel);

        JPanel equationParamsPanel = new JPanel();
        equationParamsPanel.setLayout(new BoxLayout(equationParamsPanel, BoxLayout.X_AXIS));
        equationParamsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JLabel secondDegreeLabel = new JLabel(" x^3+ ");
        secondDegreeLabel.setHorizontalAlignment(JLabel.CENTER);
        equationParamsPanel.add(secondDegreeLabel);
        JTextField secondDegreeTextField = new JTextField(String.valueOf(1));
        equationParamsPanel.add(secondDegreeTextField);

        JLabel firstDegreeLabel = new JLabel(" *x^2+ ");
        firstDegreeLabel.setHorizontalAlignment(JLabel.CENTER);
        equationParamsPanel.add(firstDegreeLabel);
        JTextField firstDegreeTextField = new JTextField(String.valueOf(2));
        equationParamsPanel.add(firstDegreeTextField);

        JLabel zeroDegreeLabel = new JLabel(" *x+ ");
        zeroDegreeLabel.setHorizontalAlignment(JLabel.CENTER);
        equationParamsPanel.add(zeroDegreeLabel);
        JTextField zeroDegreeTextField = new JTextField(String.valueOf(3));
        equationParamsPanel.add(zeroDegreeTextField);

        container.add(equationParamsPanel);
        JButton button = new JButton("Найти корни");

        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.addActionListener(e -> {
            Double secondDegree = Double.valueOf(secondDegreeTextField.getText());
            Double firstDegree = Double.valueOf(firstDegreeTextField.getText());
            Double zeroDegree = Double.valueOf(zeroDegreeTextField.getText());
            Double epsilon = Double.valueOf(epsilonTextField.getText());
            Double delta = Double.valueOf(deltaTextField.getText());
            Double minX = Double.valueOf(minXTextField.getText());
            Double maxX = Double.valueOf(maxXTextField.getText());
            ThirdDegreeEquation thirdDegreeEquation = new ThirdDegreeEquation(secondDegree,
                    firstDegree, zeroDegree, epsilon, delta);
            //thirdDegreeEquation.getRoots();
            graphic.createGraphic(thirdDegreeEquation, minX, maxX);

        });
        container.add(button);

        //frame.setPreferredSize(new Dimension(200, 100));
        frame.setMinimumSize(new Dimension(300, 245));
        frame.pack();
        frame.setVisible(true);
    }
}

